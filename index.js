

// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;

console.log(sum);

let diff = x - y;
console.log (diff);

let product = x * y;
console.log ("Product is " + product);

let quotient = x / y;
console.log ("Quotient is " +quotient);

let mod = x % y ;
console.log(mod);

// Assignment Operator

let num = 8;

num = num + 2;

console.log(num);


num += 2;
console.log(num);

num -= 2;
console.log(num);


num *= 2;
console.log(num);


num /= 2;
console.log(num);


num %= 2;
console.log(num);


// Multiple Operators And Parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;

console.log(mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);

console.log(pemdas);



let z;
let preincrement = ++z;
let postincrement = z++;
let predecrement = --z;
let postdecrement = z--;




let numA = '10';
let numB = 12;

let coercion = numA + numB;

console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let noncoercion = numC + numD;
console.log(noncoercion);
console.log(typeof noncoercion);

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

// Comparison Operators

let juan = "juan";

// Equality operator (==)


console.log( 1 == 1);
console.log( 1 == 2);
console.log( 1 === '1');
console.log( 0 == false);
console.log( 'juan' == 'juan');
console.log( 'juan' == juan);

// Inequality operator (!=)

console.log( 1 != 1);
console.log( 1 != 2);

// Strict Equality operator (===)

console.log( 'juan' == 'juan');
console.log( 1 === '1');
console.log( 0 === false);

// Strict Inequality operator (!==)

console.log( 'juan' !== 'juan');
console.log( 1 !== '1');
console.log( 0 !== false);



// Relational Operator  > and <;


let a = 50;
let b = 65;


let c = a > b;

console.log(c);

c = a < b;

console.log(c);

c = a >= b;

console.log(c);


c = a <= b;

console.log(c);


//  && , || , !



let isMarried = true;

let withCar = false;


let personG = isMarried && withCar

console.log(personG);

personG = isMarried || withCar

console.log(personG);

personG = !isMarried

console.log(personG);


